# -*- coding: utf-8 -*-
from typing import List
import itertools
from nnf import Var, Or, And, NNF, false, pysat
import sys

class Cardinality_Constraint_Generator:

    def __init__(self, vars:List[Var]):
        """
        Initializing internal variables
        """
        self.vars = vars
        self.var_labels = {(var.name):val for val, var in enumerate(self.vars, start = 1)}
        self.n_vars = len(self.vars)

    def gen_aux_vars(self, k:int):
        """
        Generalizing auxiliary variables to encode the cardinality constraint
        """
        offset = max(self.var_labels.values()) + 1
        self.aux_vars = [[Var((offset + self.n_vars + 1 + i + self.n_vars*j)) for j in range(k)] for i in range(self.n_vars)]
        self.var_labels.update({(var.name):val
                                for val, var
                                in enumerate(list(itertools.chain.from_iterable(self.aux_vars)), start= offset + self.n_vars + 1)})
        #self.n_vars += len(self.aux_vars)


    def at_most(self, k:int) -> NNF:
        """
        Generate the cardinality constraints ≤k(x1, …, xn), i.e.
        at most k of n propositional variables or formulae are true
        """
        #print(f's={s}')
        #print(f'var_labels={self.var_labels}')
        #case i = 1
        self.gen_aux_vars(k)
        sentences = []
        sentences.append(Or({~self.vars[0], self.aux_vars[0][0]}))
        for j in range(1, k): #for 1 < j ≤ k
            sentences.append(Or({~self.aux_vars[0][j]})) #Append Or to meet CNF format

        for i in range(0, self.n_vars - 1): #for 1 < i < n
            #breakpoint()
            #print(f'i ={i}')
            sentences.append(Or({~self.vars[i], self.aux_vars[i][0]}))
            sentences.append(Or({~self.aux_vars[i][0], self.aux_vars[i+1][0]}))
            for j in range(0, k - 1): # for 1 < j ≤ k
                #print(f'i ={i}, j={j}')
                sentences.append(Or({~self.vars[i+1], ~self.aux_vars[i][j], self.aux_vars[i+1][j+1]}))
                sentences.append(Or({~self.aux_vars[i][j+1], self.aux_vars[i+1][j+1]}))
            sentences.append(Or({ ~self.vars[i+1], ~self.aux_vars[i][k-1]}))

        sentences.append(Or({~self.vars[self.n_vars-1], ~self.aux_vars[self.n_vars-2][k-1]}))


        return And(sentences)


    def at_least(self, k:int) -> NNF:
        """
        Generate the cardinality constraints ≥k(x1, …, xn), i.e.
        at least k of n propositional variables or formulae are true
        According to the papper ≥k(x1, …, xn) <=> ≤(n-k)(¬x1, …, ¬xn)}
        """
        self.vars = [~v for v in self.vars]
        sentence = self.at_most(self.n_vars - k)
        return sentence

    def equal(self, k:int) -> NNF:
        """
        Generate the cardinality constraints =k(x1, …, xn), i.e.
        exactly k of n propositional variables or formulae are true
        According to the papper =k(x1, …, xn) <=> ≤(k)(¬x1, …, ¬xn)} and ≥(k)(¬x1, …, ¬xn)}
        """
        at_most_sentence = list(self.at_most(k).children)
        at_least_sentence = list(self.at_least(k).children)
        return And(at_least_sentence + at_most_sentence)

    def solve(self, sentence):
        """
        Solve CNF sencences
        """
        self.solutions= [{var.name:sol[var.name] for var in self.vars} for sol in pysat.models(sentence)]
        return self.solutions

    def solve_CNF(self, sentence):
        sols = [[self.var_labels[var.name] * (1 if sol[var.name] else -1) for var in self.vars] for sol in pysat.models(sentence)]
        return set(map(tuple,sols)) #remove duplicated solutions

    def get_solution_values(self):
        """
        Calculate the number of solutions' variables are true
        """
        return [sum([var for var in sol.values()]) for sol in self.solutions]
