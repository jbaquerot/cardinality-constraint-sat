# -*- coding: utf-8 -*-

from .context import Cardinality_Constraint_Generator
from nnf import Var, NNF, dimacs, pysat
from typing import List
import io

import unittest

class BasicTestSuite(unittest.TestCase):
    """Basic test cases."""

    def test_parseDIMACS1(self):
        problemFile = "./testdata/simple_cnf01.cnf"
        f = open(problemFile, "r", encoding= "utf-8")
        formula = dimacs.load(f)
        f.close()
        self.assertTrue(formula.is_CNF())

    def test_at_most_01(self):
        k = 1
        A, B, C = Var(1), Var(2), Var(3)
        gen = Cardinality_Constraint_Generator(vars= [A, B, C])
        sentence = gen.at_most(k)
        problemFile = "./testdata/simple_output01.cnf"
        f = open(problemFile, "w", encoding = "utf-8")
        dimacs.dump(sentence, f, mode= 'cnf', var_labels = gen.var_labels)
        f.close()
        self.assertTrue(sentence.is_CNF())
        solutions = gen.solve(sentence)
        print(f'at most 1 over 3 vars solutions CNF = {gen.solve_CNF(sentence)}')
        values = gen.get_solution_values()
        self.assertEqual(max(values), 1)

    def test_at_most_02(self):
        k = 2
        A, B, C = Var(1), Var(2), Var(3)
        gen = Cardinality_Constraint_Generator(vars= [A, B, C])
        sentence = gen.at_most(k)
        problemFile = "./testdata/simple_output02.cnf"
        f = open(problemFile, "w", encoding = "utf-8")
        dimacs.dump(sentence, f, mode= 'cnf', var_labels= gen.var_labels)
        f.close()
        self.assertTrue(sentence.is_CNF())
        solutions = gen.solve(sentence)
        #print(f'solutions={solutions}')
        print(f'at most 2 over 3 vars solutions CNF = {gen.solve_CNF(sentence)}')
        values = gen.get_solution_values()
        #print(f'values= {values}')
        self.assertEqual(max(values), 2)

    def test_at_most_03(self):
        k = 3
        A, B, C = Var(1), Var(2), Var(3)
        gen = Cardinality_Constraint_Generator(vars= [A, B, C])
        sentence = gen.at_most(k)
        problemFile = "./testdata/simple_output03.cnf"
        f = open(problemFile, "w", encoding = "utf-8")
        dimacs.dump(sentence, f, mode= 'cnf', var_labels= gen.var_labels)
        f.close()
        self.assertTrue(sentence.is_CNF())
        solutions = gen.solve(sentence)
        print(f'at most 3 over 3 vars solutions CNF = {gen.solve_CNF(sentence)}')
        values = gen.get_solution_values()
        self.assertEqual(max(values), 3)

    def test_at_most_01_of_04(self):
        k = 1
        A, B, C, D = Var('A'), Var('B'), Var('C'), Var('D')
        gen = Cardinality_Constraint_Generator(vars= [A, B, C, D])
        sentence = gen.at_most(k)
        problemFile = "./testdata/simple_output01_04.cnf"
        f = open(problemFile, "w", encoding = "utf-8")
        dimacs.dump(sentence, f, mode= 'cnf', var_labels= gen.var_labels)
        f.close()
        self.assertTrue(sentence.is_CNF())
        solutions = gen.solve(sentence)
        print(f'at most 1 over 4 vars solutions CNF = {gen.solve_CNF(sentence)}')
        values = gen.get_solution_values()
        self.assertEqual(max(values), 1)

    def test_at_most_02_of_04(self):
        k = 2
        A, B, C, D = Var('A'), Var('B'), Var('C'), Var('D')
        gen = Cardinality_Constraint_Generator(vars= [A, B, C, D])
        sentence = gen.at_most(k)
        problemFile = "./testdata/simple_output02_04.cnf"
        f = open(problemFile, "w", encoding = "utf-8")
        dimacs.dump(sentence, f, mode= 'cnf', var_labels= gen.var_labels)
        f.close()
        self.assertTrue(sentence.is_CNF())
        solutions = gen.solve(sentence)
        print(f'at most 2 over 4 vars solutions CNF = {gen.solve_CNF(sentence)}')
        values = gen.get_solution_values()
        self.assertEqual(max(values), 2)

    def test_at_least_02_of_04(self):
        k = 2
        A, B, C, D = Var('A'), Var('B'), Var('C'), Var('D')
        gen = Cardinality_Constraint_Generator(vars= [A, B, C, D])
        sentence = gen.at_least(k)
        problemFile = "./testdata/simple_output_at_least_02_04.cnf"
        f = open(problemFile, "w", encoding = "utf-8")
        dimacs.dump(sentence, f, mode= 'cnf', var_labels= gen.var_labels)
        f.close()
        self.assertTrue(sentence.is_CNF())
        solutions = gen.solve(sentence)
        print(f'at least 2 over 4 vars solutions CNF = {gen.solve_CNF(sentence)}')
        values = gen.get_solution_values()
        self.assertEqual(min(values), 2)

    def test_at_least_03_of_04(self):
        k = 3
        A, B, C, D = Var('A'), Var('B'), Var('C'), Var('D')
        gen = Cardinality_Constraint_Generator(vars= [A, B, C, D])
        sentence = gen.at_least(k)
        problemFile = "./testdata/simple_output_at_least_03_04.cnf"
        f = open(problemFile, "w", encoding = "utf-8")
        dimacs.dump(sentence, f, mode= 'cnf', var_labels= gen.var_labels)
        f.close()
        self.assertTrue(sentence.is_CNF())
        solutions = gen.solve(sentence)
        print(f'at least 3 over 4 vars solutions CNF = {gen.solve_CNF(sentence)}')
        values = gen.get_solution_values()
        self.assertEqual(min(values), 3)

    def test_at_least_01_of_04(self):
        k = 1
        A, B, C, D = Var('A'), Var('B'), Var('C'), Var('D')
        gen = Cardinality_Constraint_Generator(vars= [A, B, C, D])
        sentence = gen.at_least(k)
        problemFile = "./testdata/simple_output_at_least_01_04.cnf"
        f = open(problemFile, "w", encoding = "utf-8")
        dimacs.dump(sentence, f, mode= 'cnf', var_labels= gen.var_labels)
        f.close()
        self.assertTrue(sentence.is_CNF())
        solutions = gen.solve(sentence)
        print(f'at least 1 over 4 vars solutions CNF = {gen.solve_CNF(sentence)}')
        values = gen.get_solution_values()
        self.assertEqual(min(values), 1)

    def test_at_equal_01_of_04(self):
        k = 1
        A, B, C, D = Var('A'), Var('B'), Var('C'), Var('D')
        gen = Cardinality_Constraint_Generator(vars= [A, B, C, D])
        sentence = gen.equal(k)
        #print(f'sentence={sentence}')
        problemFile = "./testdata/simple_output_equal_01_04.cnf"
        f = open(problemFile, "w", encoding = "utf-8")
        dimacs.dump(sentence, f, mode= 'cnf', var_labels= gen.var_labels)
        #print(f'gen.var_labels={gen.var_labels}')
        f.close()
        self.assertTrue(sentence.is_CNF())
        solutions = gen.solve(sentence)
        print(f'at equal 1 over 4 vars solutions CNF = {gen.solve_CNF(sentence)}')
        #print(f'solutions={solutions}')
        values = gen.get_solution_values()
        #print(f'values= {values}')
        self.assertNotEqual(len(values), 0)
        self.assertListEqual(values, len(values)*[k])

    def test_at_equal_02_of_04(self):
        k = 2
        A, B, C, D = Var('A'), Var('B'), Var('C'), Var('D')
        gen = Cardinality_Constraint_Generator(vars= [A, B, C, D])
        sentence = gen.equal(k)
        #print(f'sentence={sentence}')
        problemFile = "./testdata/simple_output_equal_02_04.cnf"
        f = open(problemFile, "w", encoding = "utf-8")
        dimacs.dump(sentence, f, mode= 'cnf', var_labels= gen.var_labels)
        #print(f'gen.var_labels={gen.var_labels}')
        f.close()
        self.assertTrue(sentence.is_CNF())
        solutions = gen.solve(sentence)
        print(f'at equal 2 over 4 vars solutions CNF = {gen.solve_CNF(sentence)}')
        #print(f'solutions={solutions}')
        values = gen.get_solution_values()
        #print(f'values= {values}')
        self.assertNotEqual(len(values), 0)
        self.assertListEqual(values, len(values)*[k])

    def test_at_equal_03_of_04(self):
        k = 3
        A, B, C, D = Var('A'), Var('B'), Var('C'), Var('D')
        gen = Cardinality_Constraint_Generator(vars= [A, B, C, D])
        sentence = gen.equal(k)
        #print(f'sentence={sentence}')
        problemFile = "./testdata/simple_output_equal_03_04.cnf"
        f = open(problemFile, "w", encoding = "utf-8")
        dimacs.dump(sentence, f, mode= 'cnf', var_labels= gen.var_labels)
        #print(f'gen.var_labels={gen.var_labels}')
        f.close()
        self.assertTrue(sentence.is_CNF())
        solutions = gen.solve(sentence)
        print(f'at equal 3 over 4 vars solutions CNF = {gen.solve_CNF(sentence)}')
        #print(f'solutions={solutions}')
        values = gen.get_solution_values()
        #print(f'values= {values}')
        self.assertNotEqual(len(values), 0)
        self.assertListEqual(values, len(values)*[k])



if __name__ == '__main__':
    unittest.main()
