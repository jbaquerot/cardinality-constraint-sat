# Cardinality Constraints for SAT

This project is the implementation of algorithm that, given a cardinality constraint, generates a conjunctive normal form in DIMACS format.

This implementation follow the method explained at [Carsten Sinz,  Towards an optimal encoding of Boolean Cardinality Constraints. Technical report, University of Tübinguen]
