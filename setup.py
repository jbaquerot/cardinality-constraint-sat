# -*- coding: utf-8 -*-

# Learn more: https://github.com/kennethreitz/setup.py

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='ccSAT',
    version='0.1.0',
    description='Implemantation of Cardinality Constraints for SAT',
    long_description=readme,
    author='José Carlos Baquero',
    author_email='jbaquerot@gmail.com',
    url='https://gitlab.com/jbaquerot/cardinality-constraint-sat',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
